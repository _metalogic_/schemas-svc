package xml

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"

	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/schemas-svc"
)

const (
	listschemas = "/usr/local/bin/schemas.sh"
	xmllint     = "/usr/bin/xmllint"
)

// XML implements the validation interface via execs of xmllint
type XML struct {
	schemasPath string
	command     string
	info        map[string]string
}

// New creates a new Docker shell service and sets the user and scripts directory
func New(schemasPath string) (svc *XML, err error) {
	svc = &XML{
		schemasPath: schemasPath,
		command:     xmllint,
		info:        make(map[string]string),
	}
	svc.info["Type"] = "schemas-svc"
	svc.info["Version"] = "v1"

	if _, err = os.Stat(schemasPath); err != nil {
		log.Fatal(err)
	}
	return svc, err
}

func (svc *XML) ID() string {
	return "xmllint"
}

// Deploy service with buildTag to stack
func (svc *XML) Validate(doc string) (validationJSON string, err error) {

	const pat = `urn:org:pesc:message:(.*?)"`

	re := regexp.MustCompile(pat)
	match := re.FindStringSubmatch(doc)

	if match == nil {
		return validationJSON, fmt.Errorf("failed to parse XML schema")
	}

	schema := fmt.Sprintf("%s.xsd", strings.Replace(match[1], ":", "_", 1))

	log.Debugf("schema: %s", schema)

	if _, err = os.Stat(svc.schemasPath); err != nil {
		return validationJSON, err
	}

	cmd := &exec.Cmd{
		Path: svc.command,
		Dir:  svc.schemasPath,
		Args: []string{svc.command, "--schema", schema, "--path", svc.schemasPath, "--noout", "-"},
	}

	cmd.Stdin = strings.NewReader(doc)

	log.Debugf("running command: %+v", cmd)
	var output []byte
	output, err = cmd.CombinedOutput()

	var valid *schemas.Validation
	if err == nil {
		valid = &schemas.Validation{
			Output: fmt.Sprintf("document is valid for %s", schema),
		}
	} else {
		valid = &schemas.Validation{
			Output: string(output),
			Error:  err.Error(),
		}
	}

	data, err := json.MarshalIndent(valid, "", "  ")
	if err != nil {
		return validationJSON, err
	}
	validationJSON = string(data)
	return validationJSON, nil
}

// Schemas returns the the releases defined for a given stack name
func (svc *XML) Schemas() (schemasJSON string, err error) {

	if _, err = os.Stat(svc.schemasPath); err != nil {
		return schemasJSON, err
	}

	cmd := &exec.Cmd{
		Path: listschemas,
		Dir:  svc.schemasPath,
		Args: []string{listschemas, svc.schemasPath},
	}

	log.Debugf("running command: %+v", cmd)
	var output []byte
	output, err = cmd.CombinedOutput()
	if err != nil {
		return schemasJSON, err
	}

	return string(output), nil
}

func (svc *XML) Schema(schemaName string) (schema string, err error) {

	schemaFile := filepath.Join(svc.schemasPath, schemaName)
	if _, err = os.Stat(schemaFile); err != nil {
		return schema, err
	}

	contents, err := os.ReadFile(schemaFile)
	if err != nil {
		return schema, err
	}
	return string(contents), nil
}

// Common service endpoints

// Close - do we need one?
func (svc *XML) Close() {
}

// Health checks to see if shall access is available.
func (svc *XML) Health() error {
	return nil
}

// Info return information about the Service.
func (svc *XML) Info() (info map[string]string) {
	return svc.info
}
