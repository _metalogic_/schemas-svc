## Overview

The Deployment API has endpoints for viewing and managing stacks and their services in Docker Swarm.
By default two stacks are defined: apps-stack for running applications services and sys-stack for
system services (eg Traefik, forward-auth, log aggregation, etc).

## Conventions

The Deployment API is configured at startup with a root directory where the EducationPlannerBC/docker-swarm project
has been cloned.
Correct execution of the API depends on the directory structure and file naming conventions of that
project:

* all stacks must be defined in a subdirectory of the root with name ${stack}-stack
* each stack directory must include the following files and directories:
  - ${stack}-stack.tmpl
  - symlink to ../env
  - subdirectory config
  - subdirectory releases
  - manifest files named manifest-${env}-current.json for each environment defining the services and their Docker images to be deployed to the environment
* each stack directory must include a releases directory containing symbolic links to each current manifest as well as tagged release manifests that
have been deployed to PRD
* each stack directory must include a config directory with a (possibly empty) config.in file which initializes required for some services; the name of
a service config directory must agree with the service name defined in the stack template file
* the file secrets.in in the project root is sourced by deployment scripts to configure the
Docker secrets that services may use

Consult the README in the EducationPlannerBC/docker-swarm project for additional details.

## Scripts

In addition to the Docker Swarm API, two shell scripts are embedded in the image:

* deploy.sh
* releases.sh

These scripts are run in a Go cmd.Exec to satisfy some endpoint requests. Copies of the scripts are also
available on the Docker Swarm manager nodes for manual use by the devops user.


## Database procedures
There are no database tables or functions used by Deployment API.

## Configuration

Note the following important dependencies between the Docker image epbc/deployment-api and the host servers
it runs on.

A non-root user is defined to run the commands that depend on Docker. That user account needs permission
to write to the stack directories where it runs and permission to connect to the Docker socket that it uses.
To this end, it is important that the UID of the container user and the GID of Docker in the container
agree with those UIDs/GIDs on the host server.

