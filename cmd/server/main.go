package main

// @title Schemas Service
// @description.markdown
// @termsOfService https://metalogicsoftware.ca/terms/
// @contact.name API Developer Support
// @contact.url https://metalogicsoftware.ca/info
// @contact.email dev@roderickmorrison.net
// @Schemes http https
// @BasePath /schemas-api/v1
// @query.collection.format multi

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/schemas-svc/build"
	"bitbucket.org/_metalogic_/schemas-svc/docs"
	"bitbucket.org/_metalogic_/schemas-svc/server"
	"bitbucket.org/_metalogic_/schemas-svc/xml"
)

var (
	env         string
	info        *build.ProjectInfo
	traceHeader string
	levelFlg    log.Level
	portFlg     string
)

func init() {

	env = strings.ToLower(config.IfGetenv("ENV", "dev"))

	flag.Var(&levelFlg, "level", "set level of logging by the API")
	flag.StringVar(&portFlg, "p", ":8080", "HTTP management listen port")

	var err error
	info, err = build.Info()
	if err != nil {
		log.Fatalf("get project info failed: %s", err)
	}

	version := info.String()
	command := info.Name()

	flag.Usage = func() {
		fmt.Printf("Project %s:\n\n", version)
		fmt.Printf("Usage: %s -help (this message) | %s [options]:\n\n", command, command)
		flag.PrintDefaults()
	}

	docs.SwaggerInfo.Host = config.MustGetenv("APIS_HOST")
	projTemplate := config.MustGetConfig("OPENAPI_PROJECT_TEMPLATE")
	version, err = info.Format(projTemplate)
	if err != nil {
		log.Warningf("Failed to format openapi version from template %s: %s", projTemplate, err.Error())
	} else {
		docs.SwaggerInfo.Description = fmt.Sprintf("%s%s", version, docs.SwaggerInfo.Description)
	}
}

func main() {
	flag.Parse()

	traceHeader = config.IfGetenv("TRACE_HEADER_NAME", "X-Trace-Header")

	if levelFlg != log.None {
		log.SetLevel(levelFlg)
	} else {
		loglevel := os.Getenv("LOG_LEVEL")
		if loglevel == "DEBUG" {
			log.SetLevel(log.DebugLevel)
		}
	}

	schemasPath := config.IfGetenv("SCHEMAS_PATH", "/data/schemas")

	log.Infof("starting schemas-svc with schemas path: %s", schemasPath)

	xmlValidator, err := xml.New(schemasPath)
	if err != nil {
		log.Fatal(err)
	}

	exitDone := &sync.WaitGroup{}
	exitDone.Add(1)

	scmSrv := server.StartSchemasServer(portFlg, traceHeader, xmlValidator, exitDone)

	log.Infof("schemas-svc started with %s validation", xmlValidator.ID())

	// Wait for a SIGINT (perhaps triggered by user with CTRL-C) or SIGTERM (from Docker)
	// Run cleanup when signal is received
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)
	<-stop

	log.Warning("schemas-svc received stop signal - shutting down services")

	// now close the servers gracefully ("shutdown")
	var ctx context.Context
	var cancel context.CancelFunc

	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	log.Warning("stopping schemas validation server")
	scmSrv.Shutdown(ctx)

	// wait for goroutines started in StartEventServer() to stop
	exitDone.Wait()

	log.Warning("schemas validation server shutdown complete - exiting")
}
