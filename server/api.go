package server

import (
	"io/ioutil"
	"net/http"

	. "bitbucket.org/_metalogic_/glib/http" // dot import fo avoid package prefix in reference (shutup lint)
	schemas "bitbucket.org/_metalogic_/schemas-svc"
)

// @Tags Validation endpoints
// @Summary returns a handler for returning a the contents of a named schema document
// @Description returns a handler for returning a the contents of a named schema document
// @ID schema
// @Produce json
// @Param schema path string true "schema name"
// @Success 200 {string} string Plaintext
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
func Schema(svc schemas.Validator) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		schema := params["schema"]
		xml, err := svc.Schema(schema)
		if err != nil {
			ErrJSON(w, err)
			return
		}

		OkXML(w, xml)
	}
}

// @Tags Validation endpoints
// @Summary returns a handler for returning a the list of schema documents in schemasPath
// @Description returns a handler for returning a the list of schema documents in schemasPath
// @ID schemas
// @Produce json
// @Success 200 {string} string Plaintext
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
func Schemas(svc schemas.Validator) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		schemasJSON, err := svc.Schemas()
		if err != nil {
			ErrJSON(w, err)
			return
		}

		OkJSON(w, schemasJSON)
	}
}

// @Tags Validation endpoints
// @Summary returns a handler for broadcasting events to containers in the swarm
// @Description returns a handler for broadcasting events to containers in the swarm
// @ID broadcast
// @Produce json
// @Param body body deploy.Validate true "the service and URL to broadcast"
// @Success 200 {object} Message
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
func Validate(svc schemas.Validator) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		bytes, err := ioutil.ReadAll(r.Body)

		if err != nil {
			ErrJSON(w, err)
			return
		}

		validationJSON, err := svc.Validate(string(bytes))
		if err != nil {
			ErrJSON(w, err)
			return
		}

		OkJSON(w, validationJSON)
	}
}
