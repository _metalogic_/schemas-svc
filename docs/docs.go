// Package docs GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag
package docs

import (
	"bytes"
	"encoding/json"
	"strings"
	"text/template"

	"github.com/swaggo/swag"
)

var doc = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "termsOfService": "https://educationplannerbc.ca/terms/",
        "contact": {
            "name": "API Developer Support",
            "url": "https://educationplannerbc.ca/support",
            "email": "techsupport@educationplannerbc.ca"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/admin/loglevel": {
            "get": {
                "description": "gets the service log level (one of Error, Info, Debug or Trace)",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Admin endpoints"
                ],
                "summary": "gets the current service log level",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/http.Message"
                        }
                    }
                }
            }
        },
        "/admin/loglevel/{verbosity}": {
            "put": {
                "description": "dynamically sets the service log level to one of Error, Info, Debug or Trace",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Admin endpoints"
                ],
                "summary": "sets the service log level",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Log level",
                        "name": "verbosity",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/http.Message"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/deployments": {
            "get": {
                "description": "handles a Docker Swarm deployment request",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Deployment endpoints"
                ],
                "summary": "handles a Docker Swarm deployment request",
                "operationId": "get-deployments",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/deploy.Deployment"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/deployments/:service": {
            "get": {
                "description": "gets a Docker Swarm service detail",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Deployment endpoints"
                ],
                "summary": "gets a Docker Swarm service detail",
                "operationId": "get-deployment",
                "parameters": [
                    {
                        "type": "string",
                        "description": "service name",
                        "name": "service",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/deploy.Deployment"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/health": {
            "get": {
                "description": "checks health of deployment-api service",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Common endpoints"
                ],
                "summary": "check health of deployment-api service",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "503": {
                        "description": "Service Unavailable",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/info": {
            "get": {
                "description": "get deployment-api service info, including version, log level",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Common endpoints",
                    "Common endpoints"
                ],
                "summary": "gets deployment-api service info",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/build.Runtime"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/secrets": {
            "get": {
                "description": "returns the list of secrets in the Docker Swarm",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Secrets endpoints"
                ],
                "summary": "returns the list of secrets in the Docker Swarm",
                "operationId": "get-secrets",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/deploy.Secret"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            },
            "post": {
                "description": "create a new secret in the Swarm",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Secrets endpoints"
                ],
                "summary": "create a new secret in the Swarm",
                "operationId": "post-secret",
                "parameters": [
                    {
                        "description": "the secret to create",
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/deploy.SecretRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/deploy.Secret"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/secrets/:id": {
            "get": {
                "description": "get secret metadata (excludes secret value)",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Secrets endpoints"
                ],
                "summary": "get secret metadata",
                "operationId": "get-secret",
                "parameters": [
                    {
                        "type": "string",
                        "description": "the secret id",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "description": "the new secret value",
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/deploy.SecretRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/deploy.Secret"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            },
            "put": {
                "description": "update the value of a secret",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Secrets endpoints"
                ],
                "summary": "update the value of a secret",
                "operationId": "put-secret",
                "parameters": [
                    {
                        "type": "string",
                        "description": "the secret id",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "description": "the secret to create",
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/deploy.SecretRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/http.Message"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            },
            "delete": {
                "description": "remove a secret from the Swarm",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Secrets endpoints"
                ],
                "summary": "remove a secret from the Swarm",
                "operationId": "delete-secret",
                "parameters": [
                    {
                        "type": "string",
                        "description": "the secret id",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/http.Message"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/stacks": {
            "get": {
                "description": "returns the list of stacks in the Docker Swarm",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Deployment endpoints"
                ],
                "summary": "returns the list of stacks in the Docker Swarm",
                "operationId": "get-stacks",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/deploy.Stack"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/stacks/:stack/deployments": {
            "post": {
                "description": "handles a Docker Swarm deployment request",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Deployment endpoints"
                ],
                "summary": "handles a Docker Swarm deployment request",
                "operationId": "post-deployment",
                "parameters": [
                    {
                        "description": "the service and tag to deploy",
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/deploy.DeployRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/deploy.Deployment"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/stacks/:stack/releases": {
            "get": {
                "description": "returns the list of releases for a given stack",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Deployment endpoints"
                ],
                "summary": "returns the list of releases for a given stack",
                "operationId": "get-releases",
                "parameters": [
                    {
                        "type": "string",
                        "description": "stack name",
                        "name": "stack",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/stacks/:stack/releases/:release": {
            "get": {
                "description": "returns the contents of a release file for a given stack",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Deployment endpoints"
                ],
                "summary": "gets a given release for a given stack",
                "operationId": "get-release",
                "parameters": [
                    {
                        "type": "string",
                        "description": "stack name",
                        "name": "stack",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "release filename",
                        "name": "release",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/deploy.ServiceDeployment"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "build.ProjectInfo": {
            "type": "object",
            "properties": {
                "branch": {
                    "type": "string"
                },
                "command": {
                    "type": "string"
                },
                "commit": {
                    "type": "string"
                },
                "dependencies": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "project": {
                    "type": "string"
                },
                "timestamp": {
                    "type": "integer"
                }
            }
        },
        "build.Runtime": {
            "type": "object",
            "properties": {
                "logLevel": {
                    "type": "string"
                },
                "projectInfo": {
                    "$ref": "#/definitions/build.ProjectInfo"
                },
                "serviceInfo": {
                    "type": "object",
                    "additionalProperties": {
                        "type": "string"
                    }
                }
            }
        },
        "deploy.Broadcast": {
            "type": "object",
            "properties": {
                "service": {
                    "type": "string"
                },
                "url": {
                    "type": "string"
                }
            }
        },
        "deploy.Deploy": {
            "type": "object",
            "properties": {
                "enabled": {
                    "type": "boolean"
                },
                "export": {
                    "type": "string"
                },
                "image": {
                    "type": "string"
                },
                "tag": {
                    "type": "string"
                }
            }
        },
        "deploy.DeployRequest": {
            "type": "object",
            "properties": {
                "buildTag": {
                    "type": "string"
                },
                "serviceName": {
                    "type": "string"
                }
            }
        },
        "deploy.Deployment": {
            "type": "object",
            "properties": {
                "buildTag": {
                    "type": "string"
                },
                "created": {
                    "type": "string"
                },
                "environment": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "error": {
                    "type": "string"
                },
                "hosts": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "image": {
                    "type": "string"
                },
                "output": {
                    "type": "string"
                },
                "serviceName": {
                    "type": "string"
                },
                "stack": {
                    "type": "string"
                },
                "updated": {
                    "type": "string"
                }
            }
        },
        "deploy.Secret": {
            "type": "object",
            "properties": {
                "created": {
                    "type": "string"
                },
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "updated": {
                    "type": "string"
                },
                "value": {
                    "type": "string"
                },
                "version": {
                    "type": "integer"
                }
            }
        },
        "deploy.SecretRequest": {
            "type": "object",
            "properties": {
                "data": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                }
            }
        },
        "deploy.ServiceDeployment": {
            "type": "object",
            "properties": {
                "deployment": {
                    "$ref": "#/definitions/deploy.Deploy"
                },
                "service": {
                    "type": "string"
                }
            }
        },
        "deploy.Stack": {
            "type": "object",
            "properties": {
                "dir": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "releases": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/deploy.ServiceDeployment"
                    }
                }
            }
        },
        "http.ErrorResponse": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                },
                "timestamp": {
                    "type": "integer"
                }
            }
        },
        "http.Message": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                }
            }
        }
    }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Schemes     []string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = swaggerInfo{
	Version:     "",
	Host:        "",
	BasePath:    "/deployment-api/v1",
	Schemes:     []string{"http", "https"},
	Title:       "Deployment API",
	Description: "## Overview\n\nThe Deployment API has endpoints for viewing and managing stacks and their services in Docker Swarm.\nBy default two stacks are defined: apps-stack for running applications services and sys-stack for\nsystem services (eg Traefik, forward-auth, log aggregation, etc).\n\n## Conventions\n\nThe Deployment API is configured at startup with a root directory where the EducationPlannerBC/docker-swarm project\nhas been cloned.\nCorrect execution of the API depends on the directory structure and file naming conventions of that\nproject:\n\n* all stacks must be defined in a subdirectory of the root with name ${stack}-stack\n* each stack directory must include the following files and directories:\n  - ${stack}-stack.tmpl\n  - symlink to ../env\n  - subdirectory config\n  - subdirectory releases\n  - manifest files named manifest-${env}-current.json for each environment defining the services and their Docker images to be deployed to the environment\n* each stack directory must include a releases directory containing symbolic links to each current manifest as well as tagged release manifests that\nhave been deployed to PRD\n* each stack directory must include a config directory with a (possibly empty) config.in file which initializes required for some services; the name of\na service config directory must agree with the service name defined in the stack template file\n* the file secrets.in in the project root is sourced by deployment scripts to configure the\nDocker secrets that services may use\n\nConsult the README in the EducationPlannerBC/docker-swarm project for additional details.\n\n## Scripts\n\nIn addition to the Docker Swarm API, two shell scripts are embedded in the image:\n\n* deploy.sh\n* releases.sh\n\nThese scripts are run in a Go cmd.Exec to satisfy some endpoint requests. Copies of the scripts are also\navailable on the Docker Swarm manager nodes for manual use by the devops user.\n\n\n## Database procedures\nThere are no database tables or functions used by Deployment API.\n\n## Configuration\n\nNote the following important dependencies between the Docker image epbc/deployment-api and the host servers\nit runs on.\n\nA non-root user is defined to run the commands that depend on Docker. That user account needs permission\nto write to the stack directories where it runs and permission to connect to the Docker socket that it uses.\nTo this end, it is important that the UID of the container user and the GID of Docker in the container\nagree with those UIDs/GIDs on the host server.\n\n",
}

type s struct{}

func (s *s) ReadDoc() string {
	sInfo := SwaggerInfo
	sInfo.Description = strings.Replace(sInfo.Description, "\n", "\\n", -1)

	t, err := template.New("swagger_info").Funcs(template.FuncMap{
		"marshal": func(v interface{}) string {
			a, _ := json.Marshal(v)
			return string(a)
		},
		"escape": func(v interface{}) string {
			// escape tabs
			str := strings.Replace(v.(string), "\t", "\\t", -1)
			// replace " with \", and if that results in \\", replace that with \\\"
			str = strings.Replace(str, "\"", "\\\"", -1)
			return strings.Replace(str, "\\\\\"", "\\\\\\\"", -1)
		},
	}).Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, sInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
