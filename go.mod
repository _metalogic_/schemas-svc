module bitbucket.org/_metalogic_/schemas-svc

go 1.17

require (
	bitbucket.org/_metalogic_/build v0.6.1
	bitbucket.org/_metalogic_/config v1.4.0
	bitbucket.org/_metalogic_/glib v0.9.12
	bitbucket.org/_metalogic_/httptreemux-swagger v1.1.0
	bitbucket.org/_metalogic_/log v1.4.1
	github.com/dimfeld/httptreemux/v5 v5.2.2
	github.com/swaggo/swag v1.7.1
)

require (
	bitbucket.org/_metalogic_/color v1.0.4 // indirect
	bitbucket.org/_metalogic_/colorable v1.0.3 // indirect
	bitbucket.org/_metalogic_/gogit v0.0.0-20210207005759-7fdb8c8ed0f4 // indirect
	bitbucket.org/_metalogic_/isatty v1.0.4 // indirect
	bitbucket.org/_metalogic_/mmap v0.0.0-20210207003405-1e2136f1733c // indirect
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/PuerkitoBio/purell v1.1.1 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/go-openapi/jsonpointer v0.19.5 // indirect
	github.com/go-openapi/jsonreference v0.19.5 // indirect
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/go-openapi/swag v0.19.14 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2 // indirect
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
	golang.org/x/text v0.3.6 // indirect
	golang.org/x/tools v0.1.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
