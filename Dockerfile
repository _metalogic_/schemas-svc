FROM golang:1.17 as builder

COPY ./ /build

WORKDIR /build/cmd/server

RUN go generate bitbucket.org/_metalogic_/schemas-svc/build && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o schemas-svc .

FROM metalogic/alpine:3.15

RUN apk add libxml2-utils

RUN adduser -u 25010 -g 'Metalogic Application Owner' -D metalogic

WORKDIR /home/metalogic

COPY schemas.sh /usr/local/bin

COPY --from=builder /build/cmd/server/schemas-svc .
COPY --from=builder /build/docs docs

USER metalogic

CMD ["./schemas-svc"]

HEALTHCHECK --interval=30s CMD /usr/local/bin/health http://localhost:8080/${SCHEMAS_API_BASE}/health

