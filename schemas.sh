#!/bin/sh

schemasDir=$1
if [ $# == 1 ]; then
   ls -1 ${schemasDir} | jq  --raw-input '.' | jq -s '.'
   exit $?
fi

echo "usage: schemas.sh dir"
exit 1

