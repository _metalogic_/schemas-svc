#!/bin/bash

# add /usr/local/bin to allow sudo to find envtpl with its restricted search path
export PATH=$PATH:/usr/local/bin

cmd=$(basename $0)

usage() {
  echo -e "  usage: $cmd"
  echo -e "  $1"
  exit 1
}

if [ $# !=  0 ]; then
  usage
fi

export LOG_LEVEL=DEBUG
export APIS_HOST=localhost
export OPENAPI_PROJECT_TEMPLATE="<pre>((Project))\n(branch ((Branch)), commit ((Commit)))\nbuilt at ((Built))</pre>\n\n"
export SCHEMAS_PATH=/Users/Shared/data/schemas/PESC

./server
