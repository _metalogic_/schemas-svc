package schemas

type ValidateRequest struct {
	XMLSchema string `json:"xsd"`
	Document  string `json:"document"`
}

// Validation ...
type Validation struct {
	Output string `json:"output,omitempty"`
	Error  string `json:"error,omitempty"`
}
