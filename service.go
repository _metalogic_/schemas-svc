package schemas

// Common defines the common service interface
type Common interface {
	Health() error
	Info() map[string]string
}

// Validator defines the Schemas validator interface
type Validator interface {
	Common
	Schema(name string) (schemaJSON string, err error)
	Schemas() (schemasJSON string, err error)
	Validate(document string) (validationJSON string, err error)
}
