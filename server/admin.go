package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	. "bitbucket.org/_metalogic_/glib/http" // dot import fo avoid package prefix in reference (shutup lint)
	"bitbucket.org/_metalogic_/log"
	deploy "bitbucket.org/_metalogic_/schemas-svc"
	"bitbucket.org/_metalogic_/schemas-svc/build"
)

// @Tags Common endpoints
// @Summary gets deployment-api service info
// @Description get deployment-api service info, including version, log level
// @Tags Common endpoints
// @Produce json
// @Success 200 {object} build.Runtime
// @Failure 500 {object} ErrorResponse
// @Router /info [get]
func APIInfo(svc deploy.Validator) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		projectInfo, err := build.Info()
		if err != nil {
			ErrJSON(w, NewServerError(err.Error()))
			return
		}
		type runtime struct {
			ProjectInfo *build.ProjectInfo `json:"projectInfo"`
			ServiceInfo map[string]string  `json:"serviceInfo"`
			LogLevel    string             `json:"logLevel"`
		}
		rt := &runtime{
			ProjectInfo: projectInfo,
			ServiceInfo: svc.Info(),
			LogLevel:    log.GetLevel().String(),
		}

		runtimeJSON, err := json.Marshal(rt)
		if err != nil {
			ErrJSON(w, NewServerError(err.Error()))
			return
		}
		OkJSON(w, string(runtimeJSON))
	}
}

// @Tags Common endpoints
// @Summary check health of deployment-api service
// @Description checks health of deployment-api service
// @Produce json
// @Success 200 {string} Plaintext
// @Failure 503 {object} ErrorResponse
// @Router /health [get]
func Health(svc deploy.Validator) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "text/plain")
		err := svc.Health()
		if err != nil {
			http.Error(w, http.StatusText(http.StatusServiceUnavailable)+fmt.Sprintf(": %s", err), http.StatusServiceUnavailable)
			return
		}
		fmt.Fprint(w, "ok\n")
	}
}

// @Tags Admin endpoints
// @Summary gets the current service log level
// @Description gets the service log level (one of Error, Info, Debug or Trace)
// @Produce json
// @Success 200 {object} Message
// @Router /admin/loglevel [get]
func LogLevel(svc deploy.Validator) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		MsgJSON(w, log.GetLevel().String())
	}
}

// @Tags Admin endpoints
// @Summary sets the service log level
// @Description dynamically sets the service log level to one of Error, Info, Debug or Trace
// @Produce json
// @Param verbosity path string true "Log level"
// @Success 200 {object} Message
// @Failure 400 {object} ErrorResponse
// @Router /admin/loglevel/{verbosity} [put]
func SetLogLevel(svc deploy.Validator) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		var (
			verbosity string
		)

		verbosity = params["verbosity"]
		verbosity = strings.ToLower(verbosity)

		var v log.Level
		switch verbosity {
		case "error":
			v = log.ErrorLevel
		case "info":
			v = log.InfoLevel
		case "debug":
			v = log.DebugLevel
		case "trace":
			v = log.TraceLevel
		default:
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid log level: %s", verbosity)))
			return
		}

		log.SetLevel(v)
		MsgJSON(w, v.String())
	}
}
